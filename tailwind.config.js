/** @type {import('tailwindcss').Config} */
export default {
  content: ['./public/**/*.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    screens: {
      sm: '640px',
      // => @media (min-width: 640px) { ... }

      md: '768px',
      // => @media (min-width: 768px) { ... }

      lg: '1024px',
      // => @media (min-width: 1024px) { ... }

      xl: '1280px',
      // => @media (min-width: 1280px) { ... }

      '2xl': '1536px'
      // => @media (min-width: 1536px) { ... }
    },
    fontFamily: {
      montserrat: ['Montserrat', 'sans-serif']
    },
    extend: {
      animation: {
        'gradient-x': 'gradient-x 3s ease infinite',
        'gradient-y': 'gradient-y 3s ease infinite',
        'gradient-xy': 'gradient-xy 3s ease infinite'
      },
      keyframes: {
        'gradient-y': {
          '0%, 100%': {
            'background-size': '50% 50%',
            'background-position': 'center top'
          },
          '50%': {
            'background-size': '600% 600%',
            'background-position': 'center center'
          }
        },
        'gradient-x': {
          '0%, 100%': {
            'background-size': '600% 600%',
            'background-position': 'left center'
          },
          '50%': {
            'background-size': '600% 600%',
            'background-position': 'right center'
          }
        },
        'gradient-xy': {
          '0%, 100%': {
            'background-size': '50% 50%',
            'background-position': 'left center'
          },
          '50%': {
            'background-size': '600% 600%',
            'background-position': 'right center'
          }
        }
      }
    }
  },
  plugins: []
}
