import { defineStore } from 'pinia'
import { createClient } from '@supabase/supabase-js'

// Create a single supabase client for interacting with your database
const supabaseUrl = 'https://nodkgqpjvkdhvnlbwqqu.supabase.co'
const supabaseAnonPublic = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6Im5vZGtncXBqdmtkaHZubGJ3cXF1Iiwicm9sZSI6ImFub24iLCJpYXQiOjE2ODExMzM1OTksImV4cCI6MTk5NjcwOTU5OX0.Y1bG119f4yK5W7aeMceVn4Ynf7nsqLPg4hzY1IYKAfs'
const supabase = createClient(supabaseUrl, supabaseAnonPublic)

const supabaseTables = {
  1: 'field-work',
  2: 'general-view-test',
  3: 'general-view-users',
  4: 'projects',
  5: 'users'
}

export const useMainStore = defineStore('main', () => {
  // --- Methods ---
  const request = async (params) => {
    try {
      const data = await fetch(params.endpoint, { ...params.configs })
      return Promise.resolve(data)
    } catch (error) {
      return Promise.reject(error)
    }
  }

  const supabaseRequest = async (params) => {
    const { data, error } = await supabase
      .from(supabaseTables[params.tableIndex])
      .select()
      .order(params.orderBy, { ascending: true })

    if (error) return error
    return data
  }

  const supabasePost = async (params) => {
    const { error } = await supabase
      .from(supabaseTables[params.tableIndex])
      .insert(params.data)

    if (error) console.log(error)
  }

  const supabaseDelete = async (params) => {
    const { error } = await supabase
      .from(supabaseTables[params.tableIndex])
      .delete()
      .eq('id', params.id)

    if (error) console.log(error)
  }

  return {
    request,
    supabaseRequest,
    supabasePost,
    supabaseDelete,
  }
})
