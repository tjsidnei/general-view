import { initializeApp } from 'firebase/app';
import { getDatabase, ref, set, onValue } from "firebase/database";

// TODO: Replace the following with your app's Firebase project configuration
const firebaseConfig = {
  apiKey: "AIzaSyDmgVjmL8-YgGqoI8f34VQs7ppq8mtVqao",
  authDomain: "hash-game-8d711.firebaseapp.com",
  databaseURL: "https://hash-game-8d711.firebaseio.com",
  projectId: "hash-game-8d711",
  storageBucket: "hash-game-8d711.appspot.com",
  messagingSenderId: "477565299381",
  appId: "1:477565299381:web:8e8aa40be8ce595883ecfc",
  measurementId: "G-QY9HZ9YBWM"
};

const firebaseObj = initializeApp(firebaseConfig);
const database = getDatabase(firebaseObj);

export default {
  database,
  ref,
  set,
  onValue
}