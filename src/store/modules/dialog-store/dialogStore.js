import { defineStore } from 'pinia'
import { ref } from 'vue'

export const useDialogStore = defineStore('dialog', () => {
  const display = ref({})
  const params = ref({
    name: 'noName',
    title: 'Missing tilte',
    message: 'Missing message',
    reinforcement: null,
    cancelBtnType: 'red',
    cancelBtnLabel: 'Cancel',
    cancelBtn: null,
    actionBtnType: 'green',
    actionBtnLabel: 'Confirm',
    actionBtn: null
  })

  return {
    display,
    params
  }
})
