import { defineStore } from 'pinia'
import { ref } from 'vue'
import firebaseObj from '@/store/modules/firebase/firebase.js'

export const useTicTacToeStore = defineStore('ticTacToe', () => {
  const ticTacToeData = ref({})

  // --- Methods ---
  const getTicTacToeData = () => {
    const starCountRef = firebaseObj.ref(firebaseObj.database, 'hash_game/');
    firebaseObj.onValue(starCountRef, (snapshot) => {
      ticTacToeData.value = snapshot.val()
    });
  }

  const updateSelectedPlayer = (payload) => {
    firebaseObj.set(
      firebaseObj.ref(
        firebaseObj.database, `hash_game/players/${payload.key}/isSelected`
      ),
      payload.isSelected
    )
  }

  const resetGame = (payload) => {
    firebaseObj.set(
      firebaseObj.ref(
        firebaseObj.database, `hash_game/`
      ),
      payload
    )
  }

  const selectButton = (payload) => {
    firebaseObj.set(
      firebaseObj.ref(
        firebaseObj.database, `hash_game/${payload.key}`
      ),
      {
        player: payload.playerSelected.name,
        label: payload.playerSelected.label
      }
    )
  }

  return {
    getTicTacToeData,
    ticTacToeData,
    updateSelectedPlayer,
    resetGame,
    selectButton
  }
})
