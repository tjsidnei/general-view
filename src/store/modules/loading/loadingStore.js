import { defineStore } from 'pinia'
import { ref, computed } from 'vue'

export const useLoadingStore = defineStore('loading', () => {
  const partialLoadings = ref([])
  const globalLoadings = ref([])

  const isPartialLoading = computed(() => {
    return partialLoadings.value.length !== 0
  })

  const isGlobalLoading = computed(() => {
    return globalLoadings.value.length !== 0
  })

  const startGlobalLoading = (key) => {
    globalLoadings.value = [...globalLoadings.value, key]
  }

  const startPartialLoading = (key) => {
    partialLoadings.value = [...partialLoadings.value, key]
  }

  const endGlobalLoading = (key) => {
    globalLoadings.value = globalLoadings.value.filter((globalLoading) => globalLoading !== key)
  }

  const endPartialLoading = (key) => {
    partialLoadings.value = partialLoadings.value.filter((partialLoading) => partialLoading !== key)
  }

  return {
    partialLoadings,
    globalLoadings,
    isGlobalLoading,
    isPartialLoading,
    startGlobalLoading,
    startPartialLoading,
    endGlobalLoading,
    endPartialLoading
  }
})
