import { defineStore } from 'pinia'
import { useMainStore } from '@/store/mainStore.js'

export const useTestStore = defineStore('test', () => {
  const mainStore = useMainStore()

  // --- Methods ---
  const getTestData = async (params) => {
    return await mainStore.supabaseRequest(params)
  }

  const postTestData = async (params) => {
    await mainStore.supabasePost(params)
  }

  return {
    getTestData,
    postTestData
  }
})
