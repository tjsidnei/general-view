import { defineStore } from 'pinia'
import { useMainStore } from '@/store/mainStore.js'
import { useLoadingStore } from '@/store/modules/loading/loadingStore'
import { ref } from 'vue'

export const useManageUsersStore = defineStore('manageUsers', () => {
  const mainStore = useMainStore()
  const loadingStore = useLoadingStore()
  const usersData = ref([])

  // --- Methods ---
  const getAllUsers = async (params) => {
    loadingStore.startPartialLoading('getAllUsers')
    if (!params.orderBy) params.orderBy = 'first_name'

    try {
      usersData.value = await mainStore.supabaseRequest(params)
    } catch (error) {
      console.log(error)
    } finally {
      loadingStore.endPartialLoading('getAllUsers')
    }
  }

  const postUserData = async (params) => {
    loadingStore.startPartialLoading('postUserData')
    
    try {
      await mainStore.supabasePost(params)
    } catch (error) {
      console.log(error)
    } finally {
      loadingStore.endPartialLoading('postUserData')
    }
  }
  
  const deleteUser = async (params) => {
    loadingStore.startPartialLoading('deleteUserData')
    
    try {
      await mainStore.supabaseDelete(params)
    } catch (error) {
      console.log(error)
    } finally {
      loadingStore.endPartialLoading('deleteUserData')
    }
  }

  return {
    postUserData,
    usersData,
    getAllUsers,
    deleteUser
  }
})
