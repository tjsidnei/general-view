import { createRouter, createWebHistory } from 'vue-router'
import Main from '../views/general/main.vue'
import testsRoutes from './modules/tests/testsRoutes.js'
import manageUsers from './modules/manage-users/manageUsersRoutes.js'
import ticTacToeRoutes from './modules/tic-tac-toe/ticTacToeRoutes.js'

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      name: 'Main',
      path: '/',
      component: Main,
      redirect: { name: 'Home' },
      children: [
        ...testsRoutes,
        ...manageUsers,
        ...ticTacToeRoutes,
        {
          path: 'home',
          name: 'Home',
          component: () => import('@/views/general/Home.vue')
        }
      ]
    }
  ]
})

export default router
