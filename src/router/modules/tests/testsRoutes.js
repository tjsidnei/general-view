const testsRoutes = [
  {
    path: '/tests',
    component: () => import('@/views/tests/TestPage.vue'),
    name: 'Tests'
  }
]

export default testsRoutes
