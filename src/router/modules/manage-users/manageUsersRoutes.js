const manageUsersRoutes = [
  {
    path: '/manage-users',
    name: 'ManageUsers',
    component: () => import('@/views/manage-users/manageUsers.vue')
  }
]

export default manageUsersRoutes
