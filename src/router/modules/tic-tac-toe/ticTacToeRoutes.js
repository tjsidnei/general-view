const ticTacToeRoutes = [
  {
    path: '/tic-tac-toe',
    name: 'TicTacToe',
    component: () => import('@/views/tic-tac-toe/ticTacToe.vue')
  }
]

export default ticTacToeRoutes
